Column(
  mainAxisSize: MainAxisSize.max,
  children: [
    Image.network(
      'https://picsum.photos/seed/336/600',
      width: MediaQuery.of(context).size.width,
      height: 100,
      fit: BoxFit.cover,
    ),
  ],
)

