Column(
  mainAxisSize: MainAxisSize.max,
  children: [
    TextFormField(
      controller: textController1,
      onChanged: (_) => EasyDebounce.debounce(
        'textController1',
        Duration(milliseconds: 2000),
        () => setState(() {}),
      ),
      autofocus: true,
      obscureText: false,
      decoration: InputDecoration(
        labelText: 'username',
        hintText: 'username',
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
      ),
      style: FlutterFlowTheme.of(context).bodyText1,
    ),
    Divider(),
    TextFormField(
      controller: textController2,
      onChanged: (_) => EasyDebounce.debounce(
        'textController2',
        Duration(milliseconds: 2000),
        () => setState(() {}),
      ),
      autofocus: true,
      obscureText: false,
      decoration: InputDecoration(
        labelText: 'email',
        hintText: 'email',
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
      ),
      style: FlutterFlowTheme.of(context).bodyText1,
    ),
    TextFormField(
      controller: textController3,
      onChanged: (_) => EasyDebounce.debounce(
        'textController3',
        Duration(milliseconds: 2000),
        () => setState(() {}),
      ),
      autofocus: true,
      obscureText: !passwordVisibility,
      decoration: InputDecoration(
        labelText: 'password',
        hintText: 'password',
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x00000000),
            width: 1,
          ),
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        suffixIcon: InkWell(
          onTap: () => setState(
            () => passwordVisibility = !passwordVisibility,
          ),
          focusNode: FocusNode(skipTraversal: true),
          child: Icon(
            passwordVisibility
                ? Icons.visibility_outlined
                : Icons.visibility_off_outlined,
            color: Color(0xFF757575),
            size: 22,
          ),
        ),
      ),
      style: FlutterFlowTheme.of(context).bodyText1,
    ),
    FFButtonWidget(
      onPressed: () {
        print('Button pressed ...');
      },
      text: 'update',
      options: FFButtonOptions(
        width: 130,
        height: 40,
        color: FlutterFlowTheme.of(context).primaryColor,
        textStyle: FlutterFlowTheme.of(context).subtitle2.override(
              fontFamily: 'Poppins',
              color: Colors.white,
            ),
        borderSide: BorderSide(
          color: Colors.transparent,
          width: 1,
        ),
        borderRadius: 12,
      ),
    ),
  ],
)

